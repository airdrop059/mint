const { db } = require('../database/database');
const { bot } = require('../server/server');





exports.Addprofile = async function Addprofile(msg) {
  const chatId = msg.chat.id;

  try {
    const propertyIds = '';
    const cookie = await bot.sendMessage(chatId, `cookie: `, { reply_markup: { force_reply: true } })
    bot.onReplyToMessage(chatId, cookie.message_id, async (message) => {
      const cookie = message.text;
      const passcode = await bot.sendMessage(chatId, `passcode: `, { reply_markup: { force_reply: true } });
      bot.onReplyToMessage(chatId, passcode.message_id, async (message) => {
        const passcode = message.text;
        db.run(`INSERT INTO profile (telegram_id, priceto, cookie, propertyIds, address, passcode, currency) VALUES (?, ?, ?, ?, ?, ?, ?)`, [chatId, 'priceto', cookie, propertyIds, 'address', passcode, 'crrency'], function (err) {
          if (err) {
            console.log(err)
            return bot.sendMessage(chatId, `profile gagal ditambahkan`, {
              reply_markup: {
                keyboard: [
                  ['DELETE', 'UPDATE'], ['BACK']
                ],
                resize_keyboard: true,
              }
            });
          } else {
            return bot.sendMessage(chatId, `profile berhasil ditambahkan`, {
              reply_markup: {
                keyboard: [
                  ['DELETE', 'UPDATE'], ['BACK']
                ],
                resize_keyboard: true,
              }
            });
          }
        });

      })
    });
  } catch (e) {
    console.log(e.message, 'error Addprofile');
  }
}

exports.Profile = async function Profile(msg) {
  const chatId = msg.chat.id;

  try {
    db.get(`SELECT * FROM profile WHERE telegram_id = ?`, [chatId], (err, row) => {
      if (err) {
        return console.error(err.message, 'error baca SELECT * FROM profile');
      } else if (row) {
        const profile = { telegram_id: row.telegram_id, priceto: row.priceto, cookie: row.cookie, propertyIds: row.propertyIds, address: row.address, passcode: row.passcode, currency: row.currency };
        return bot.sendMessage(chatId, `cookie: ${profile.cookie.slice(0, 10)}.....${profile.cookie.substring(profile.cookie.length - 10)}\npasscode: ${profile.passcode}`, {
          reply_markup: {
            keyboard: [
              ['DELETE', 'UPDATE'], ['BACK']
            ],
            resize_keyboard: true,
          }
        });
      } else {
        return bot.sendMessage(chatId, `Profile tidak ditemukan. buat profile di /profile`,);
      }
    });

  } catch (e) {
    return console.log('error baca Profile', e.message);
  }
}

exports.DeleteProfile = async function DeleteProfile(msg) {
  const chatId = msg.chat.id;

  try {
    db.run(`DELETE FROM profile WHERE telegram_id=${chatId}`, (err) => {
      if (err) {
        return console.log('erro di DELETE FROM profile WHERE', err.message);
      } else {
        return bot.sendMessage(chatId, `profile berhasil dihapus`, {
          reply_markup: {
            keyboard: [
              ['DELETE', 'UPDATE'], ['BACK']
            ],
            resize_keyboard: true,
          }
        });
      }
    })
  } catch (e) {
    return console.log('error delete profile', e.message);
  }
}

exports.UpdateProfile = async function UpdateProfile(msg) {
  const chatId = msg.chat.id;

  try {
    const cookie = await bot.sendMessage(chatId, `cookie: `, { reply_markup: { force_reply: true } })
    bot.onReplyToMessage(chatId, cookie.message_id, async (message) => {
      const cookie = message.text;
      const passcode = await bot.sendMessage(chatId, `passcode: `, { reply_markup: { force_reply: true } });
      bot.onReplyToMessage(chatId, passcode.message_id, async (message) => {
        const passcode = message.text;
        db.run(`UPDATE profile SET cookie = ?, passcode = ? WHERE telegram_id = ?`, [cookie, passcode, chatId], (err) => {
          if (err) {
            return bot.sendMessage(chatId, `profile gagal diupdate`, {
              reply_markup: {
                keyboard: [
                  ['DELETE', 'UPDATE'], ['BACK']
                ],
                resize_keyboard: true,
              }
            });
          } else {
            return bot.sendMessage(chatId, `profile berhasil diupdate`, {
              reply_markup: {
                keyboard: [
                  ['DELETE', 'UPDATE'], ['BACK']
                ],
                resize_keyboard: true,
              }
            });
          }
        });
      })
    })
  } catch (e) {
    return console.log('error update profile', e.message);
  }
}

