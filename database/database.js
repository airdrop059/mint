const sqlite3 = require('sqlite3').verbose();


const db = new sqlite3.Database('users.db', (err) => {
  if (err) {
    console.error("eror coonect to database", err.message);
  }
  console.log('Terhubung ke database users.db.');
});

function getData(chatId) {
  return new Promise((resolve, reject) => {
      db.get(`SELECT * FROM data WHERE telegram_id = ?`, [chatId], (err, row) => {
          if (err) {
              reject(err);
          } else {
              resolve(row);
          }
      });
  });
}

function getProfile(chatId) {
  return new Promise((resolve, reject) => {
      db.get(`SELECT * FROM profile WHERE telegram_id = ?`, [chatId], (err, row) => {
          if (err) {
              reject(err);
          } else {
              resolve(row);
          }
      });
  });
}

db.run(`CREATE TABLE IF NOT EXISTS profile (
  telegram_id INTEGER,
  priceto TEXT,
  cookie TEXT,
  propertyIds TEXT,
  address TEXT,
  passcode TEXT,
  currency TEXT
)`);

db.run(`CREATE TABLE IF NOT EXISTS data (
  telegram_id INTEGER,
  stopflag TEXT,
  marketid TEXT,
  sessionkey TEXT,
  chatid TEXT
)`);

module.exports = { db, getData, getProfile };
