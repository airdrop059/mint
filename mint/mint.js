const { bot } = require('../server/server');
const { db, getData, getProfile } = require('../database/database');
const axios = require('axios');




exports.Mint = async function Mint(msg) {
    const chatId = msg.chat.id;

    db.get(`SELECT * FROM profile WHERE telegram_id = ?`, [chatId], async (err, row) => {
        if (err) {
            return console.error(err.message, 'error baca SELECT * FROM profile');
        } else if (row) {
            const marketid = await bot.sendMessage(msg.chat.id, 'masukan id market: ', { reply_markup: { force_reply: true } });
            bot.onReplyToMessage(msg.chat.id, marketid.message_id, async (message) => {
                const marketid = message.text

                db.run(`INSERT INTO data (telegram_id, stopflag, marketid) VALUES (?, ?, ?)`, [chatId, 1, marketid]);
                bot.sendMessage(msg.chat.id, `proses... `, { reply_markup: { inline_keyboard: [[{ text: 'CANCEL', callback_data: 'STOP' }]] } }).then((sent) => { db.run(`UPDATE data SET chatid = ? WHERE telegram_id = ?`, [sent.message_id, chatId]); });
                const profile = await getProfile(chatId);

                const start = await pay(chatId, profile.cookie, marketid, profile.passcode);
                if (start == 'stop') {
                    const data = await getData(chatId);
                    bot.deleteMessage(chatId, data.chatid);
                    db.run(`UPDATE data SET stopflag= ? WHERE telegram_id = ?`, [1, chatId])
                    return bot.sendMessage(chatId, `CANCEL`, { reply_markup: { keyboard: [['BACK']], resize_keyboard: true } });
                } else if (start == 'success') {
                    const data = await getData(chatId);
                    bot.deleteMessage(chatId, data.chatid);
                    return bot.sendMessage(chatId, `MINTED`, { reply_markup: { inline_keyboard: [[{ text: 'STOP', callback_data: `${data.sessionkey}` }]] } }).then((sent) => { db.run(`UPDATE data SET chatid = ? WHERE telegram_id = ?`, [sent.message_id, chatId]); });
                }

            })
        } else {
            return bot.sendMessage(chatId, `Profile tidak ditemukan. buat profile di /profile`,);
        }
    });
}



exports.StopBuy = async function Stop(msg) {
    const chatId = msg.chat.id;

    try {
        db.run(`UPDATE data SET stopflag = ? WHERE telegram_id = ?`, [0, chatId]);

        const profile = await getProfile(chatId);
        const cancele = await Cancele(chatId, profile.cookie);
        if (cancele == null) {
            const data = await getData(chatId);
            bot.deleteMessage(chatId, data.chatid);
            return bot.sendMessage(chatId, `error`, { reply_markup: { keyboard: [['MINT', 'BACK']], resize_keyboard: true } });
        } else {
            const data = await getData(chatId);
            bot.deleteMessage(chatId, data.chatid);
            bot.sendMessage(chatId, `STOPED`, { reply_markup: { keyboard: [['MINT', 'BACK']], resize_keyboard: true } });
            return db.run(`DELETE FROM data WHERE sessionkey="${data.sessionkey}"`)
        }

    } catch (e) {
        return console.log('error StopBuy', e.message);
    }
}


async function OpenMarket(chatId, cookie, marketid) {
    while (true) {
        const data = await getData(chatId);
        if (data.stopflag == 1) {
            try {
                await GetSlod(cookie,marketid)
                const response = await axios.post(
                    `https://citizen.store.dosi.world/api/stores/v2/payment/drops/${marketid}`,
                    {
                        'currency': 'USD',
                        'currencyCodeExchangedTo': 'FNSA',
                        'callbackUrl': {
                            'onApprovedUrl': 'https://citizen.store.dosi.world/purchase/approveCryptoPurchase?isC2C=false',
                            'cancelUrl': 'https://citizen.store.dosi.world/purchase/cancelPurchase'
                        }
                    },
                    {
                        headers: {
                            'authority': 'citizen.store.dosi.world',
                            'accept': 'application/json',
                            'accept-language': 'id-ID,id;q=0.9,en-US;q=0.8,en;q=0.7',
                            'content-type': 'application/json',
                            'cookie': cookie,
                            'origin': 'https://citizen.store.dosi.world',
                            'referer': `https://citizen.store.dosi.world/en-US/1st_sale/sales/${marketid}?currency=FNSA`,
                            'sec-ch-ua': '"Google Chrome";v="113", "Chromium";v="113", "Not-A.Brand";v="24"',
                            'sec-ch-ua-mobile': '?0',
                            'sec-ch-ua-platform': '"Windows"',
                            'sec-fetch-dest': 'empty',
                            'sec-fetch-mode': 'cors',
                            'sec-fetch-site': 'same-origin',
                            'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/113.0.0.0 Safari/537.36'
                        }
                    }
                );
                // console.log(response.data.responseData)
                if (response.data.responseData.paymentId) {
                    return response.data.responseData.paymentId
                }
            } catch (error) {
                console.log('error in OpenMarket', error.message)
            }
        } else if (data.stopflag == 0) {
            return 'stop'
        } else {
            return 'error'
        }
    }
}

async function session(chatId, cookie, marketid, passcode) {
    const market = await OpenMarket(chatId, cookie, marketid);
    if (market == 'stop') {
        return 'stop'
    } else if (market == 'error') {
        return 'error'
    } else {
        try {
            await Passcode(cookie, passcode);
            const response = await axios.post(
                `https://wallet.dosi.world/api/v1/payments/crypto/${market}/session`,
                {
                    'currentIpCountryCodeAlpha2': 'ID'
                },
                {
                    headers: {
                        'Accept-Language': 'id-ID,id;q=0.9,en-US;q=0.8,en;q=0.7',
                        'Connection': 'keep-alive',
                        'Cookie': cookie,
                        'Origin': 'https://wallet.dosi.world',
                        'Referer': 'https://wallet.dosi.world/purchase/crypto-ln/12a86b21-252e-43d0-9f3d-6c3dbf59e5be',
                        'Sec-Fetch-Dest': 'empty',
                        'Sec-Fetch-Mode': 'cors',
                        'Sec-Fetch-Site': 'same-origin',
                        'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/111.0.0.0 Safari/537.36',
                        'sec-ch-ua': '"Google Chrome";v="111", "Not(A:Brand";v="8", "Chromium";v="111"',
                        'sec-ch-ua-mobile': '?0',
                        'sec-ch-ua-platform': '"Windows"',
                        'sentry-trace': '240b7ebf6fe646e8a683f48e8872f946-8eac78df4f4b60a3-0'
                    }
                }
            );
            if (response.data.responseData) {
                return response.data.responseData.sessionKey
            }
        } catch (error) {
            console.log('error in session', error.message)
        }
    }
}


async function pay(chatId, cookie, marketid, passcode) {
    while (true) {
        const data = await getData(chatId);
        if (data.stopflag == 1) {
            const sessionKey = await session(chatId, cookie, marketid, passcode)
            if (sessionKey == 'stop') {
                return 'stop'
            } else if (sessionKey == 'error') {
                return 'error'
            } else {
                try {
                    const bilingaddress = (await axios.get(`https://wallet.dosi.world/api/v1/payments/crypto/session/${sessionKey}`, {
                        headers: {
                            'Accept': 'application/json, text/plain, */*',
                            'Accept-Language': 'id-ID,id;q=0.9,en-US;q=0.8,en;q=0.7',
                            'Connection': 'keep-alive',
                            'Cookie': cookie,
                            'Referer': 'https://wallet.dosi.world/payment/purchase/crypto-ln/78926a64-7c55-4a40-a87a-9974b2119990',
                            'Sec-Fetch-Dest': 'empty',
                            'Sec-Fetch-Mode': 'cors',
                            'Sec-Fetch-Site': 'same-origin',
                            'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/111.0.0.0 Safari/537.36',
                            'sec-ch-ua': '"Google Chrome";v="111", "Not(A:Brand";v="8", "Chromium";v="111"',
                            'sec-ch-ua-mobile': '?0',
                            'sec-ch-ua-platform': '"Windows"',
                            'sentry-trace': 'b78b37a591454cb6aca3a66ff8ae7912-88506ae1240bed64-1'
                        }
                    })).data.responseData.billingCountry.countryCodeAlpha2

                    const response = await axios.post(
                        `https://wallet.dosi.world/api/v1/payments/crypto/${sessionKey}/start`,
                        {
                            'buyerCryptoWalletAddress': 'link180sayalxm8rg9kgztef9guhh0d4annkj8m92nx',
                            'billingAddress': bilingaddress,
                            'currentBlockNumber': 53968263
                        },
                        {
                            headers: {
                                'Accept-Language': 'id-ID,id;q=0.9,en-US;q=0.8,en;q=0.7',
                                'Connection': 'keep-alive',
                                'Cookie': cookie,
                                'Origin': 'https://wallet.dosi.world',
                                'Referer': 'https://wallet.dosi.world/purchase/crypto-ln/12a86b21-252e-43d0-9f3d-6c3dbf59e5be',
                                'Sec-Fetch-Dest': 'empty',
                                'Sec-Fetch-Mode': 'cors',
                                'Sec-Fetch-Site': 'same-origin',
                                'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/111.0.0.0 Safari/537.36',
                                'sec-ch-ua': '"Google Chrome";v="111", "Not(A:Brand";v="8", "Chromium";v="111"',
                                'sec-ch-ua-mobile': '?0',
                                'sec-ch-ua-platform': '"Windows"'
                            }
                        }
                    );
                    // console.log(response.data.responseCode)
                    if (response.data.responseCode == '200') {
                        db.run(`UPDATE data SET sessionkey=? WHERE telegram_id=?`, [sessionKey, chatId])
                        return 'success'
                    }
                } catch (error) {
                    return console.log('error in pay', error.message)
                }
            }

        } else if (data.stopflag == 0) {
            return 'stop'
        }
    }
}

async function Passcode(cookie, passcode) {
    try {
        const response = await axios.post(
            'https://wallet.dosi.world/api/v1/user/passcode/verify',
            {
                'passcode': passcode
            },
            {
                headers: {
                    'Accept-Language': 'id-ID,id;q=0.9,en-US;q=0.8,en;q=0.7',
                    'Connection': 'keep-alive',
                    'Cookie': cookie,
                    'Origin': 'https://wallet.dosi.world',
                    'Referer': 'https://wallet.dosi.world/?logout_url=https%3A%2F%2Fcitizen.dosi.world%2Fauth%2Flogout',
                    'Sec-Fetch-Dest': 'empty',
                    'Sec-Fetch-Mode': 'cors',
                    'Sec-Fetch-Site': 'same-origin',
                    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/111.0.0.0 Safari/537.36',
                    'sec-ch-ua': '"Google Chrome";v="111", "Not(A:Brand";v="8", "Chromium";v="111"',
                    'sec-ch-ua-mobile': '?0',
                    'sec-ch-ua-platform': '"Windows"'
                }
            }
        );
        if (response.data.responseCode == '200') {
            console.log('passcode success')
        }
    } catch (e) {
        console.log(e.message + ' passcode')
    }
}

async function GetSlod(cookie,marketid) {
    while (true) {
        try {
            const response = await axios.get(`https://citizen.store.dosi.world/api/stores/v2/sales/${marketid}/status`, {
                headers: {
                    'authority': 'citizen.store.dosi.world',
                    'accept': 'application/json',
                    'accept-language': 'id-ID,id;q=0.9,en-US;q=0.8,en;q=0.7',
                    'cookie': cookie,
                    'referer': `https://citizen.store.dosi.world/en-US/1st_sale/sales/${marketid}?currency=FNSA`,
                    'sec-ch-ua': '"Google Chrome";v="113", "Chromium";v="113", "Not-A.Brand";v="24"',
                    'sec-ch-ua-mobile': '?0',
                    'sec-ch-ua-platform': '"Windows"',
                    'sec-fetch-dest': 'empty',
                    'sec-fetch-mode': 'cors',
                    'sec-fetch-site': 'same-origin',
                    'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/113.0.0.0 Safari/537.36'
                }
            });
            // console.log(response.data)
            if (response.data.responseData.saleStatus == 'FOR_SALE' && response.data.responseData.reservable == true) {
                break
            }

        } catch (e) {
            console.log(e)
        }
    }
}



async function Cancele(chatId, cookie) {
    const data = await getData(chatId);
    try {
        const response = await axios.delete(`https://wallet.dosi.world/api/v1/payments/crypto/${data.sessionkey}`, {
            headers: {
                'Accept': 'application/json, text/plain, */*',
                'Accept-Language': 'id-ID,id;q=0.9,en-US;q=0.8,en;q=0.7',
                'Connection': 'keep-alive',
                'Cookie': cookie,
                'Origin': 'https://wallet.dosi.world',
                'Referer': 'https://wallet.dosi.world/purchase/crypto/33c53fb4-b54c-472f-b756-a0e67e9d1aaa',
                'Sec-Fetch-Dest': 'empty',
                'Sec-Fetch-Mode': 'cors',
                'Sec-Fetch-Site': 'same-origin',
                'User-Agent': 'Mozilla/5.0 (Linux; Android 12; ASUS_I005D) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Mobile Safari/537.36',
                'sec-ch-ua': '"Chromium";v="107", "Not=A?Brand";v="24"',
                'sec-ch-ua-mobile': '?1',
                'sec-ch-ua-platform': '"Android"'
            }
        });
        console.log(response.data)
        if (response.data.responseCode == '200') {
            return 'cancel'
        }
    } catch (e) {
        console.log(e.message + ' stop')
    }

}




// async function SessionHold(chatId, cookie, currency, saleid, passcode) {
//     try {
//         Passcode(cookie, passcode)
//         const res = await axios.post(
//             `https://citizen.store.dosi.world/api/stores/v2/payment/market/${saleid}`,
//             {
//                 'currency': currency,
//                 'callbackUrl': {
//                     'onApprovedUrl': 'https://citizen.store.dosi.world/id-ID/purchase/approveC2CPurchase',
//                     'cancelUrl': 'https://citizen.store.dosi.world/purchase/cancelPurchase'
//                 }
//             },
//             {
//                 headers: {
//                     'authority': 'citizen.store.dosi.world',
//                     'accept': 'application/json',
//                     'accept-language': 'id-ID,id;q=0.9,en-US;q=0.8,en;q=0.7',
//                     'content-type': 'application/json',
//                     'cookie': cookie,
//                     'origin': 'https://citizen.store.dosi.world',
//                     'referer': 'https://citizen.store.dosi.world/nfts/2825825',
//                     'sec-ch-ua': '"Google Chrome";v="111", "Not(A:Brand";v="8", "Chromium";v="111"',
//                     'sec-ch-ua-mobile': '?0',
//                     'sec-ch-ua-platform': '"Windows"',
//                     'sec-fetch-dest': 'empty',
//                     'sec-fetch-mode': 'cors',
//                     'sec-fetch-site': 'same-origin',
//                     'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/111.0.0.0 Safari/537.36'
//                 }
//             }
//         );
//         const response = await axios.post(
//             `https://wallet.dosi.world/api/v1/payments/crypto/${res.data.responseData.paymentId}/session`,
//             {
//                 'currentIpCountryCodeAlpha2': 'ID'
//             },
//             {
//                 headers: {
//                     'Accept-Language': 'id-ID,id;q=0.9,en-US;q=0.8,en;q=0.7',
//                     'Connection': 'keep-alive',
//                     'Cookie': cookie,
//                     'Origin': 'https://wallet.dosi.world',
//                     'Referer': 'https://wallet.dosi.world/purchase/crypto-ln/9917c6e5-e276-4de9-b740-94acc028a378',
//                     'Sec-Fetch-Dest': 'empty',
//                     'Sec-Fetch-Mode': 'cors',
//                     'Sec-Fetch-Site': 'same-origin',
//                     'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/111.0.0.0 Safari/537.36',
//                     'sec-ch-ua': '"Google Chrome";v="111", "Not(A:Brand";v="8", "Chromium";v="111"',
//                     'sec-ch-ua-mobile': '?0',
//                     'sec-ch-ua-platform': '"Windows"',
//                     'sentry-trace': '09910dd8160143ba8b3f185be0f3c868-980850be0ff82854-0'
//                 }
//             }
//         );
//         // console.log(response.data)
//         if (response.data.responseData) {
//             db.run(`UPDATE data SET sessionKey= ? WHERE telegram_id= ?`, [response.data.responseData.sessionKey, chatId]);
//             return response.data.responseData.sessionKey
//         }
//     } catch (e) {
//         console.log(e.message + ' sessionhold')
//         // console.log(e.response.data)
//     }
// }


// async function Hold(chatId) {
//     while (true) {
//         const profile = await getProfile(chatId)
//         const data = await getData(chatId);
//         try {
//             if (data.stopflag == 1) {
//                 const response = await axios.get(`https://wallet.dosi.world/api/v1/payments/crypto/session/${data.sessionkey}`, {
//                     headers: {
//                         'Accept': 'application/json, text/plain, */*',
//                         'Accept-Language': 'id-ID,id;q=0.9,en-US;q=0.8,en;q=0.7',
//                         'Connection': 'keep-alive',
//                         'Cookie': profile.cookie,
//                         'Referer': 'https://wallet.dosi.world/payment/purchase/crypto-ln/78926a64-7c55-4a40-a87a-9974b2119990',
//                         'Sec-Fetch-Dest': 'empty',
//                         'Sec-Fetch-Mode': 'cors',
//                         'Sec-Fetch-Site': 'same-origin',
//                         'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/111.0.0.0 Safari/537.36',
//                         'sec-ch-ua': '"Google Chrome";v="111", "Not(A:Brand";v="8", "Chromium";v="111"',
//                         'sec-ch-ua-mobile': '?0',
//                         'sec-ch-ua-platform': '"Windows"',
//                         'sentry-trace': 'b78b37a591454cb6aca3a66ff8ae7912-88506ae1240bed64-1'
//                     }
//                 });
//                 const start = await axios.post(
//                     `https://wallet.dosi.world/api/v1/payments/crypto/${data.sessionkey}/start`,
//                     {
//                         'buyerCryptoWalletAddress': profile.address,
//                         'billingAddress': response.data.responseData.billingCountry.countryCodeAlpha2,
//                         'currentBlockNumber': 16848067
//                     },
//                     {
//                         headers: {
//                             'Accept-Language': 'id-ID,id;q=0.9,en-US;q=0.8,en;q=0.7',
//                             'Connection': 'keep-alive',
//                             // 'Cookie': this.cookie,
//                             'Origin': 'https://wallet.dosi.world',
//                             'Referer': 'https://wallet.dosi.world/purchase/crypto/6e32c7a3-f0e5-4f66-833b-fffe7eef2a4f',
//                             'Sec-Fetch-Dest': 'empty',
//                             'Sec-Fetch-Mode': 'cors',
//                             'Sec-Fetch-Site': 'same-origin',
//                             'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/111.0.0.0 Safari/537.36',
//                             'sec-ch-ua': '"Google Chrome";v="111", "Not(A:Brand";v="8", "Chromium";v="111"',
//                             'sec-ch-ua-mobile': '?0',
//                             'sec-ch-ua-platform': '"Windows"'
//                         }
//                     }
//                 );

//                 // console.log(start.data.responseCode)
//                 if (start.data.responseCode == '200') {

//                 } else if (start.data.responseCode == '422') {
//                     await SessionHold(chatId, profile.cookie, profile.currency, data.saleid, profile.passcode)
//                     return Hold(chatId)
//                 }

//             } else if (data.stopflag == 0) {
//                 return
//             }
//         } catch (e) {

//         }
//     }
// }





