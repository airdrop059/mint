const { bot } = require('./server/server');
const { Addprofile, Profile, DeleteProfile, UpdateProfile } = require('./profile/profile');
const { Mint, StopBuy } = require('./mint/mint');
const { db, getData } = require('./database/database');




bot.onText(/\/start/, async (msg) => {
    bot.sendMessage(msg.chat.id, `Halo ${msg.chat.first_name}, selamat datang di bot ini.`, {
        reply_markup: {
            keyboard: [
                ['MINT', 'PROFILE'], ['HELP']
            ],
            resize_keyboard: true,
        }
    });
});

bot.onText(/\/profile/, async (msg) => {
    return Addprofile(msg);
});

bot.on('message', async (msg) => {
    const chatId = msg.chat.id;

    if (msg.text == 'PROFILE') {
        return Profile(msg);

    } else if (msg.text == 'MINT') {
        return Mint(msg);
    } else if (msg.text == 'DELETE') {
        return DeleteProfile(msg);
    } else if (msg.text == 'UPDATE') {
        return UpdateProfile(msg);
    } else if (msg.text == 'BACK') {
        return bot.sendMessage(msg.chat.id, `Halo ${msg.chat.first_name}, selamat datang di bot ini.`, {
            reply_markup: {
                keyboard: [
                    ['MINT', 'PROFILE'], ['HELP']
                ],
                resize_keyboard: true,
            }
        });
    } else if (msg.text == 'LIST') {
        const data = await getData(chatId)
        bot.sendMessage(chatId, `MINTED`, { reply_markup: { inline_keyboard: [[{ text: 'STOP', callback_data: `${data.sessionkey}` }]] } }).then((sent) => { db.run(`UPDATE data SET chatid = ? WHERE telegram_id = ?`, [sent.message_id, chatId]); });
    }

});

bot.on('callback_query', async (query) => {
    const chatId = query.message.chat.id;
    const text = query.data;


    const data = await getData(chatId);
    if (text == data.sessionkey) {
        return StopBuy(query.message);
    } else if (text == 'STOP') {
        db.run(`UPDATE data SET stopflag= ? WHERE telegram_id = ?`, [0, chatId])
    }
})


